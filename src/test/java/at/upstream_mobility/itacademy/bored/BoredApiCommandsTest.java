package at.upstream_mobility.itacademy.bored;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BoredApiCommandsTest {

    BoredApiClient boredApiClient;
    BoredApiCommands boredApiCommands;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUp() {
        boredApiClient = Mockito.mock(BoredApiClient.class);
        boredApiCommands = new BoredApiCommands(boredApiClient);
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testGetRandomFromApi() {
        BoredApiResponse response = new BoredApiResponse();
        response.setActivity("test");

        when(boredApiClient.fetchActivity()).thenReturn(Mono.just(response));

        Mono<String> result = boredApiCommands.getRandomFromApi();

        StepVerifier.create(result)
                .expectNext("test")
                .verifyComplete();
    }

    @Test
    public void testGetByType() {
        BoredApiResponse response = new BoredApiResponse();
        response.setActivity("test");

        when(boredApiClient.fetchActivity(anyString())).thenReturn(Mono.just(response));

        Mono<String> result = boredApiCommands.getByTypeFromApi(0);

        StepVerifier.create(result)
                .expectNext("test")
                .verifyComplete();
    }

    @Test
    public void testGetRandom() {
        BoredApiResponse boredApiResponse = new BoredApiResponse();
        boredApiResponse.setActivity("test");
        when(boredApiClient.fetchActivity()).thenReturn(Mono.just(boredApiResponse));

        boredApiCommands.getRandom();

        assertTrue(outContent.toString().contains("test"));
        System.setOut(originalOut);
    }

    @Test
    public void testGet() {
        int typeNumber = 7;
        BoredApiResponse boredApiResponse = new BoredApiResponse();
        boredApiResponse.setActivity("test");
        when(boredApiClient.fetchActivity("music")).thenReturn(Mono.just(boredApiResponse));

        boredApiCommands.get(typeNumber);

        assertTrue(outContent.toString().contains("test"));
        System.setOut(originalOut);
    }
}
