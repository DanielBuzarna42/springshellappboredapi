package at.upstream_mobility.itacademy.bored;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

@SpringBootTest
public class BoredApiClientTest {

    @Autowired
    private BoredApiClient boredApiClient;

    @Test
    public void testFetchActivity() {
        StepVerifier.create(boredApiClient.fetchActivity())
                .expectNextMatches(boredApiResponse -> boredApiResponse != null && boredApiResponse.getActivity() != null)
                .verifyComplete();
    }

    @Test
    public void testFetchActivityWithString() {
        StepVerifier.create(boredApiClient.fetchActivity("music"))
                .expectNextMatches(boredApiResponse -> boredApiResponse != null && boredApiResponse.getActivity() != null)
                .verifyComplete();
    }
}
