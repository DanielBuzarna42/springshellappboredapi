package at.upstream_mobility.itacademy.bored;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

@Component
public class BoredApiClient {
    private static final String BASE_URL = "https://www.boredapi.com/api/activity";
    private final WebClient webClient;

    @Autowired
    public BoredApiClient(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(BASE_URL).build();
    }

    public Mono<BoredApiResponse> fetchActivity() {
        return this.webClient.method(HttpMethod.GET)
                .uri(UriBuilder::build)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(BoredApiResponse.class);
    }

    public Mono<BoredApiResponse> fetchActivity(String type) {
        return this.webClient.method(HttpMethod.GET)
                .uri(uriBuilder -> uriBuilder.queryParam("type", type).build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(BoredApiResponse.class);
    }
}