package at.upstream_mobility.itacademy.bored;

import at.upstream_mobility.itacademy.bored.api.DefaultApi;
import at.upstream_mobility.itacademy.bored.api.model.ActivityGet200Response;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoredApplication {

	public static void main(String[] args) throws Exception {

		DefaultApi defaultApi = new DefaultApi();
		ActivityGet200Response response = defaultApi.activityGet();
		System.out.println(response.getActivity());

		SpringApplication.run(BoredApplication.class, args);
	}
}
