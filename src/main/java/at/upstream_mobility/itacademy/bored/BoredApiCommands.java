package at.upstream_mobility.itacademy.bored;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;


@ShellComponent
public class BoredApiCommands {

    private final BoredApiClient boredApiClient;

    private final List<String> activityTypes = Arrays.asList("education", "recreational", "social", "diy", "charity",
            "cooking", "relaxation", "music", "busywork");

    @Autowired
    public BoredApiCommands(BoredApiClient boredApiClient) {
        this.boredApiClient = boredApiClient;
    }

    @ShellMethod
    public void printList() {
        System.out.println("Available categorys:");
        for (int i = 0; i < activityTypes.size(); i++) {
            System.out.println(i + ": " + activityTypes.get(i));
        }
    }

    public Mono<String> getRandomFromApi() {
        return boredApiClient.fetchActivity().map(BoredApiResponse::getActivity);
    }

    public Mono<String> getByTypeFromApi(int typeNumber) {
        String type = activityTypes.get(typeNumber);
        return boredApiClient.fetchActivity(type).map(BoredApiResponse::getActivity);
    }

    void printResult(Mono<String> result) {
        result.subscribe(System.out::println);
    }

    @ShellMethod
    public void getRandom() {
        printResult(getRandomFromApi());
    }

    @ShellMethod
    public void get(int typeNumber) {
        printResult(getByTypeFromApi(typeNumber));
    }
}